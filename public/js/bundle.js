/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _DefineProp = __webpack_require__(/*! ../classworks/DefineProp */ \"./classworks/DefineProp.js\");\n\nvar _DefineProp2 = _interopRequireDefault(_DefineProp);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*\r\n  1. Decorator as Design Pattern -> ./decorator/index.js\r\n  2. Higher Order Functions -> ./features/hoc.js\r\n  3. Object.defineProperty(...) -> ./features/defineProperty\r\n  4. Object.assign and Spread Operator\r\n  5. ES7 Decorator -> ./decorator/es7_decorator\r\n\r\n*/\n// Decorator();\n// AssignAndRes();\n//Mediator();\n// classworks ->\n//  Work1();\n(0, _DefineProp2.default)(); //import Decorator from './decorator';\n//import AssignAndRes from './features/object.assign';\n//import Mediator from './decorator/es7_decorator';\n\n//import Work1 from '../classworks/decorator';\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/DefineProp.js":
/*!**********************************!*\
  !*** ./classworks/DefineProp.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/*\r\n  Задание:\r\n\r\n  Написать класс SuperDude который как аргумент принимает два параметра:\r\n    - Имя\r\n    - Массив суперспособностей которые являются обьектом.\r\n\r\n    Модель суперспособности:\r\n      {\r\n        // Имя способности\r\n        name:'Invisibility',\r\n        // Сообщение которое будет выведено когда способность была вызвана\r\n        spell: function(){ return `${this.name} hide from you`}\r\n      }\r\n\r\n    В конструкторе, нужно:\r\n    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то\r\n      значение которое мы передали как аргумент.\r\n\r\n    - перебрать массив способностей и на каждую из них создать метод для этого\r\n      обьекта, используя поле name как название метода, а spell как то,\r\n      что нужно вернуть в console.log при вызове этого метода.\r\n    - все способности должны быть неизменяемые\r\n\r\n    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );\r\n*/\n\nvar DefineProp = function DefineProp() {\n  var SuperDude = function SuperDude(name, superPowers) {\n    var _this = this;\n\n    _classCallCheck(this, SuperDude);\n\n    Object.defineProperty(this, 'name', {\n      value: name,\n      configurable: false,\n      writable: false,\n      enumerable: false\n    });\n    superPowers.forEach(function (e) {\n      console.log(e);\n      Object.defineProperty(_this, e.name, {\n        value: function value() {\n          e.spell();\n        },\n        configurable: false,\n        writable: false,\n        enumerable: false\n      });\n    });\n  }; //end\n\n\n  var superPowers = [{ name: 'Invisibility', spell: function spell() {\n      return this.name + ' hide from you';\n    } }, { name: 'superSpeed', spell: function spell() {\n      return this.name + ' running from you';\n    } }, { name: 'superSight', spell: function spell() {\n      return this.name + ' see you';\n    } }, { name: 'superFroze', spell: function spell() {\n      return this.name + ' will froze you';\n    } }, { name: 'superSkin', spell: function spell() {\n      return this.name + ' skin is unbreakable';\n    } }];\n\n  var Luther = new SuperDude('Luther', superPowers);\n  console.log(Luther);\n  // Тестирование: Методы должны работать и выводить сообщение.\n  Luther.superSight();\n  Luther.superSpeed();\n  Luther.superFroze();\n  Luther.Invisibility();\n  Luther.superSkin();\n};\n\nexports.default = DefineProp;\n\n//# sourceURL=webpack:///./classworks/DefineProp.js?");

/***/ })

/******/ });