/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

const BeachParty = () => {
  function Human( name ){
    this.name = name;
    this.currentTemperature = 0;
    this.maxTemperature = 50;



    console.log(`new Human ${this.name} arrived!`);
  }

  Human.prototype.ChangeTemperature = function ChangeTemperature( changeValue ){
    console.log(
      'current', this.currentTemperature + changeValue,
      'max', this.maxTemperature
    );

    this.currentTemperature = this.currentTemperature + changeValue;

    if( this.currentTemperature > this.maxTemperature ){
      console.error(`Очень жарко: ${this.currentTemperature}. ${this.name} умер :(`);

    } else {
        if (this.currentTemperature > 30) {
          if(this.coolers != 'undefined'){
            this.currentTemperature = this.currentTemperature + this.coolers[0].temperatureCoolRate;
            console.log(
              `вы сьели ${this.coolers[0].name}
              Температура теперь ${this.currentTemperature}
              `);
              this.coolers.shift();
          }
          console.log('w');
        }else {
            console.log(`На улице (${this.currentTemperature}, нужно чем то охладить ${this.name} иначе он умрет`);
        }

      }

  };

  function FunctionDecorator( Human ){
    this.name = Human.name;
    console.log(Human);
    this.currentTemperature = Human.currentTemperature;
    console.log(Human.currentTemperature);
    this.coolers = [
      {name: 'icecream', temperatureCoolRate: -5},
      {name: 'вода', temperatureCoolRate: -15},
      {name: 'лед', temperatureCoolRate: -3}
    ];
    this.maxTemperature = Human.maxTemperature;
    console.log(Human.maxTemperature);
  }
  FunctionDecorator.prototype = Human.prototype;
  var test = new FunctionDecorator( new Human('Dexter'));

  var timerId = setTimeout(function() {
    let random = randomInteger(-5, 25)
    test.ChangeTemperature(random);
  }, 1000);
  
  function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }
}//end

export default BeachParty;
